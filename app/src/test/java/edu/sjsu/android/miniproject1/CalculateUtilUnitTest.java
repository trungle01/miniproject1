package edu.sjsu.android.miniproject1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class CalculateUtilUnitTest {
    @Test
    public void calculateTax_NoTax_isCorrect1() {
        float P = 10000.00f;
        float J = (float) 5.5/1200;
        int N = 15 * 12;
        float T = 0.0f;
        float actual = CalculateUtil.calculateTax(P,J,N,T);
        float expected = 81.71f;
        assertEquals(expected, actual,0.2f);
    }
    @Test
    public void calculateTax_WithTaxisCorrect2() {
        float P = 10000.00f;
        float J = (float) 5.5/1200;
        int N = 15 * 12;
        float T = (float) 0.1*P/100;
        float actual = CalculateUtil.calculateTax(P,J,N,T);
        float expected = 91.71f;
        assertEquals(expected, actual,0.2f);
    }
    @Test
    public void calculateTax_WithTax_isCorrect3() {
        float P = 10000.00f;
        float J = (float) 10/1200;
        int N = 30 * 12;
        float T = (float) 0.1*P/100;
        float actual = CalculateUtil.calculateTax(P,J,N,T);
        float expected = 97.76f;
        assertEquals(expected, actual,0.2f);
    }

    @Test
    public void calculateTax_NoTax_isNotCorrect1() {
        float P = 20000.00f;
        float J = 0.0f;
        int N = 20 * 12;
        float T = 0.0f;
        float actual = CalculateUtil.calculateTax(P,J,N,T); // correct output = 83.33f
        float expected = 80.00f;
        assertEquals(expected, actual,0.2f);
    }
    @Test
    public void calculateTax_WithTax_isNotCorrect2() {
        float P = 20000.00f;
        float J = (float) 10/1200;
        int N = 20 * 12;
        float T = (float) 0.1*P/100;
        float actual = CalculateUtil.calculateTax(P,J,N,T); //correct output = 213.00f
        float expected = 200.00f;
        assertEquals(expected, actual,0.2f);
    }
}