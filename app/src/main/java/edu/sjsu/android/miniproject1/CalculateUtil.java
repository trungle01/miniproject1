package edu.sjsu.android.miniproject1;

import java.math.BigDecimal;
import java.math.RoundingMode;

class CalculateUtil {
    public static float calculateTax(float P, float J, int N, float T){
        float M = 0f;       // monthly payment
        if(J == 0){
            M = P/N + T;
        }
        else{       // J > 0
            M = (float) ((P*J)/(1 - Math.pow((1+J),-N)) + T);
        }
        return roundFloat(M,2);  // return a float with 2 decimal numbers
    }

    private static float roundFloat(float f, int places){
        BigDecimal bd = new BigDecimal(Float.toString(f));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.floatValue();
    }
}
