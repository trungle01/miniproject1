package edu.sjsu.android.miniproject1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import edu.sjsu.android.miniproject1.databinding.ActivityMainBinding;

// figure out what is wrong with the mortgage formulate

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int init_seekbar_progress = 100;
        int max = 200;
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        //set initial seekbar
        binding.seekBar.setMax(max);
        binding.seekBar.setProgress(init_seekbar_progress);
        binding.btnUninstall.setOnClickListener(this::uninstall);
        binding.btnCalculate.setOnClickListener(this::calculate);
        binding.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float progressPercent = ((float)progress)/10;
                binding.seekBar.setProgress(progress);
                binding.textViewPercentRate.setText(new StringBuilder().append(String.valueOf(progressPercent)).append("%").toString());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //
            }
        });
    }

    // method calculate() find the mortgage
    public void calculate(View v) {
        // validate user input not empty and up to 2 decimal places.
        String input = binding.txtPrinciple.getText().toString();
        if (!validate(input)) {
            // Show warning for enter principle
            binding.textViewAnnouncement.setText(getString(R.string.invalid_input));
            //Toast message
            Toast.makeText(this, "Invalid Input!", Toast.LENGTH_SHORT).show();
            return;
        }

        // P = Principal (the amount borrowed that the user input in the text box)
        float P = Float.parseFloat(input);
        // J = Monthly interest in decimal form (annual interest rate / 12)
        int progress = binding.seekBar.getProgress();
        float annual_interest_rate = (((float)progress) / 10)/100;
        float J = annual_interest_rate/12;
        // N = Number of months of the loan (number of years * 12)
        int N = getNumOfMonths();
        //  T = Monthly taxes and insurance = 0.1% * P (if selected)
        float T = binding.checkBox.isChecked() ? 0.1f / 100 * P : 0f;
        // find the monthly payment
        float result = CalculateUtil.calculateTax(P, J, N, T);
        System.out.println("result = " + result);
        binding.textViewAnnouncement.setText("Mortgage is: " + result + "$");
    }

    // method validates user input not empty and up to 2 decimal places.
    // DONE
    public boolean validate(String input) {
        boolean result = true;
        int decimalPlaces = 0;
        if(input.contains(".")){
            int integerPlaces = input.indexOf(".");
            decimalPlaces = input.length() - integerPlaces - 1;
        }
        // if principle is empty or has more than 2 decimal digits
        if (input.length() == 0 || decimalPlaces > 2) {
            result = false;
        }
        return result;
    }

    // method get number of months from group radio buttons
    public int getNumOfMonths() {
        if (binding.radioButton15.isChecked()) {
            return 15 * 12;
        } else if (binding.radioButton20.isChecked()) {
            return 20 * 12;
        } else {
            return 30 * 12;
        }
    }

    // method uninstall app
    public void uninstall(View v) {
        Intent delete = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName()));
        startActivity(delete);
    }

}